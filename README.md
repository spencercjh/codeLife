[![GitHub license](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

* [this page's repo](https://github.com/spencercjh/codeLife)

* [my Github](https://github.com/spencercjh)

* [my gitee](https://gitee.com/spencercjh)

**_Now, I focus on learning Kotlin in my spare time._**

**Some Docs are migrated to [Wiki](https://github.com/spencercjh/codeLife/wiki)**

## Main Works

* [a MVP Android](https://github.com/spencercjh/CrabScoreMVP)

* [a Spring Cloud Practice](https://github.com/spencercjh/SpringCloudCrabScore)

* [a web service calling wkhtmltopdf](https://github.com/spencercjh/htmlToPdf)

* [a web service doing CSRF](https://github.com/spencercjh/FakeCallOnApi)

## Code Life Links

* [a simple django demo followed official](https://github.com/spencercjh/codeLife/tree/master/django_study)

* [**a kotlin demo with complete RESTful API, JPA**](https://github.com/spencercjh/codeLife/tree/master/kotlin/spring-kotlin-restful-demo)

* [**my kotlin learning notes**](https://github.com/spencercjh/codeLife/tree/master/kotlin/kotlin-feature)

* [interview experience](https://github.com/spencercjh/codeLife/tree/master/interview)

* [online exam](https://github.com/spencercjh/codeLife/tree/master/exam/src/exam)

* [**a simple chat room with Netty**](https://github.com/spencercjh/codeLife/tree/master/netty/chat-demo)

* [**a netty demo realized JT/T 808**](https://github.com/spencercjh/codeLife/tree/master/netty/jt-808-protocol)

* [netty demos followed by _Netty In Action_](https://github.com/spencercjh/codeLife/tree/master/netty/netty)

* [a simple demo learning mongoDb](https://github.com/spencercjh/codeLife/tree/master/spring/gs-accessing-data-mongodb)

* [a simple spring security demo with webflux](https://github.com/spencercjh/codeLife/tree/master/spring/spring-boot-webflux-security)

* [demo code about cglib](https://github.com/spencercjh/codeLife/tree/master/theory/cglibDemo)

* [design patterns](https://github.com/spencercjh/codeLife/tree/master/theory/designPatterns/src)

* [**distributed ID service demo**](https://github.com/spencercjh/codeLife/tree/master/theory/distributedIdService)

* [**practice about _Java Functional Programming_**](https://github.com/spencercjh/codeLife/tree/master/theory/functional)

* [RMI example demo](https://github.com/spencercjh/codeLife/tree/master/theory/rmi-example)
